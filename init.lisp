;; -*- Mode: Lisp -*-
(in-package :stumpwm)

;;; Load Swank
(require :swank)
(swank-loader:init)
(swank:create-server :port 4004
		     :style swank:*communication-style*
		     :dont-close t)

;; Basics
(setf *startup-message* NIL
      *suppress-abort-messages* t
      *shell-program* (getenv "SHELL")
      *terminal* (getenv "TERM"))

;; -special keys
(define-keysym #x1008ff95 "XF86WLAN")
(define-keysym #x1008ff93 "XF86Battery")
(define-keysym #x1008ff13 "XF86AudioRaiseVolume")
(define-keysym #x1008ff11 "XF86AudioLowerVolume")
(define-keysym #x1008ff12 "XF86AudioMute")
;;(define-keysym #x1008ff02 "XF86MonBrightnessUp")
;;(define-keysym #x1008ff03 "XF86MonBrightnessDown")
(define-keysym #x1008ff2f "XF86Sleep")

(set-module-dir
 (pathname-as-directory (concat (getenv "HOME") "/.stumpwm.d/modules")))

;; Set up multiple groups
(run-commands "grename 1")
(run-commands "gnewbg 2")
(run-commands "gnewbg 3")
(run-commands "gnewbg 4")

(load "~/.stumpwm.d/audio.lisp")
(load "~/.stumpwm.d/keybindings.lisp")
(load "~/.stumpwm.d/visual.lisp")
