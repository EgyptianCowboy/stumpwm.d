(set-prefix-key (kbd "C-t"))

;; emacs framesplitting
(define-key *root-map* (kbd "0") "remove")
(define-key *root-map* (kbd "1") "only")
(define-key *root-map* (kbd "2") "vsplit")
(define-key *root-map* (kbd "3") "hsplit")

(define-key *top-map* (kbd "s-Delete") "remove")

(define-key *top-map* (kbd "s-f") "pull-hidden-next")
(define-key *top-map* (kbd "s-b") "pull-hidden-previous")

(define-key *top-map* (kbd "s-Up") "move-focus up")
(define-key *top-map* (kbd "s-Down") "move-focus down")
(define-key *top-map* (kbd "s-Left") "move-focus left")
(define-key *top-map* (kbd "s-Right") "move-focus right")

(define-key *top-map* (kbd "s-M-Up") "move-window up")
(define-key *top-map* (kbd "s-M-Down") "move-window down")
(define-key *top-map* (kbd "s-M-Left") "move-window left")
(define-key *top-map* (kbd "s-M-Right") "move-window right")

(define-key *top-map* (kbd "S-s-Up") "exchange-direction up")
(define-key *top-map* (kbd "S-s-Down") "exchange-direction down")
(define-key *top-map* (kbd "S-s-Left") "exchange-direction left")
(define-key *top-map* (kbd "S-s-Right") "exchange-direction right")

(define-key *root-map* (kbd "c") "exec st -e tmux")
(define-key *top-map* (kbd "s-i") "exec iridium-browser")
(define-key *top-map* (kbd "s-k") "exec kicad")
(define-key *top-map* (kbd "s-l") "exec slock")
