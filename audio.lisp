;;(define-key *top-map* (kbd "XF86WLAN") wicd)
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "exec amixer sset Master 5+")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "exec amixer sset Master 5-")
(define-key *top-map* (kbd "XF86AudioMute") "exec amixer sset Master toggle")
;;(define-key *top-map* (kbd "XF86MonBrightnessUp") "UpBrightness")
;;(define-key *top-map* (kbd "XF86MonBrightnessDown") "DownBrightness")
