(in-package :stumpwm)

;;; Visual
;;; Colors based off spacemacs-dark-theme for emacs
(let ((white "#d6d6d4")
      (grey "#303030")
      (orange "#fd971f")
      (cyan "#d7d7ff"))

  (set-fg-color white)
  (set-bg-color grey)
  (set-border-color cyan)
  (set-focus-color cyan)
  (set-unfocus-color grey)

  (setf *mode-line-foreground-color* white
	*mode-line-background-color* grey)
  (setf (car *colors*) grey
	(car (last *colors*)) white)
  (update-color-map (current-screen)))

(setf *mode-line-border-width* 0)

(setf *startup-message* nil)

(setf *message-window-gravity* :center)
(setf *input-window-gravity* :center)

(ql:quickload :clx-truetype)
(load-module :battery-portable)
(load-module :wifi)
(load-module :ttf-fonts)
(xft:cache-fonts)

(set-font (make-instance 'xft:font
			 :family "Iosevka"
			 :subfamily "Regular"
			 :size 12))

(defvar *vol-status-command*
  "amixer get Master | grep '[0-9]*\%' -o | tr -d '\\n'")

;; Show time, cpu usage and network traffic in the modelinecomment
(setf *screen-mode-line-format*
      (list "%n | "
	    '(:eval (stumpwm:run-shell-command "date +'[%a] %d/%m/%y | %H:%M' | tr -d '\\n'" t))
	    " | %B | %I | Vol: "
	    '(:eval (stumpwm:run-shell-command *vol-status-command* t))
	    " | %W"))

(setf *window-format* "%m%n%s%15t ")
(setf *wifi-modeline-fmt* "%p")

;;; When windows are desroyed window numbers are not synced
;;; 2kays <https://github.com/2kays> posted a solution on
;;; the TipsAndTricks section of the wiki
;;; This will repack window numbers every time a window is killed
 (stumpwm:add-hook stumpwm:*destroy-window-hook*
		   #'(lambda (win) (stumpwm:repack-window-numbers)))

;;(setf *screen-mode-line-format*
;;      (list "%w | %B | %I | "
;;            '(:eval (run-shell-command "date" t))))
;; Turn on the modeline
(toggle-mode-line (current-screen) (current-head))
